﻿using System;
using System.Windows.Forms;
using EAGetMail;
using RabbitMQ.Client;

using System.Text.Json;
using System.IO;
using System.Text;
using System.Collections;
using System.Threading;

namespace ProvaRabbitEmail
{
    public partial class Form1 : Form
    {

        private MyEmail currEmail;
        private IConnection connection;

        private MailServer oServer;
        private MailClient oClient;
        private string logMSG;
        private bool mailRunning = false;


        /************************************************************************
         * 
         * Prima di accedere alla mail, abilitare l'accesso per le app:
         * 
         * https://myaccount.google.com/lesssecureapps?pli=1&rapt=AEjHL4MYCAmxC43Qpi1pWHGWVFNcgJlIIiOuTPRNVVDFNQNqsIt8VYvV06eeaN6m8SCwBcR5d5nYib-6SApNl6iVDzYxK0GRyw
         * 
         * ed alla fine delle prove diabilitarlo
         * 
         ************************************************************************/

        private string myEmail = "LA TUA EMAIL";
        private string myPwd = "LA TUA PASSWORD ENCODED BASE64";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            setButton();

            logWrite("Definizione connessione Gmail");
            connectGmail();

            logWrite("Definizione connessione RabbitMQ");
            connectToRabbit();
        }

        private void setButton()
        {
            btnGetMail.Enabled = !mailRunning;
            btnStopMail.Enabled = mailRunning;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            mailRunning = true;

            if (mailRunning)
            {
                Thread trd = new Thread(new ThreadStart(this.mailThread))
                {
                    IsBackground = true
                };
                trd.Start();

                
            }

            setButton();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            mailRunning = false;

            if (oClient != null && oClient.Connected)
            {
                oClient.Quit();
            }

            setButton();
        }

        private void submitRabbitMQ()
        {
            if (connection.IsOpen)
            {

                if (currEmail != null)
                {

                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: "toNLP",
                                             durable: true,
                                             exclusive: false,
                                             autoDelete: false,
                                             arguments: null);

                        string message = JsonSerializer.Serialize(currEmail.Body.Replace("\r\n"," "));
                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish(exchange: "",
                                             routingKey: "toNLP",
                                             basicProperties: null,
                                             body: body);
                    }
                }
            }
        }

        private void mailThread()
        {
            
            while (mailRunning)
            {
                oClient = new MailClient("TryIt");

                oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                logMSG = "Tentativo di connessione al server di posta (" + myEmail + ")";
                this.Invoke(new EventHandler(logAdd));

                oClient.Connect(oServer);
                logMSG = "Connessione riuscita ... ";
                this.Invoke(new EventHandler(logAdd));

                MailInfo[] infos = oClient.GetMailInfos();

                int totMail = countMail(infos);
                
                logMSG = "Trovate " + totMail + " email da elaborare ... ";
                this.Invoke(new EventHandler(logAdd));

                int j = 1;
                for (int i = 0; i < infos.Length; i++)
                {
                    MailInfo info = infos[i];


                    // Receive email from IMAP4 server
                    Mail oMail = oClient.GetMail(info);

                    if (oMail.Subject.ToString().Trim().ToLower().Contains("prova rabbitmq"))
                    {

                        logMSG = " ... elaborazione mail " + (j).ToString() + " di " + totMail + " ... ";
                        this.Invoke(new EventHandler(logAdd));


                        currEmail = new MyEmail();
                        currEmail.SentDate = oMail.ReceivedDate.Date;
                        currEmail.Sender = oMail.From.Address.ToString();
                        currEmail.Object = oMail.Subject.ToString();
                        currEmail.Body = oMail.TextBody;

                        string jsonString = JsonSerializer.Serialize(currEmail);
                        File.WriteAllText(@"D:\RABBIT\tmp\Email_" + currEmail.Sender.Replace("@", "_").Replace(".", "_") + "_" + ("00" + j.ToString()).PadRight(2) + ".json", jsonString);


                        logMSG = " ... inserimento nella coda del messaggio " + (j).ToString() + " di " + totMail + " ... ";
                        this.Invoke(new EventHandler(logAdd));

                        for (int t = 0; t < 5; t++)
                        {
                            submitRabbitMQ();
                        }


                        if (!info.Read)
                        {
                            oClient.MarkAsRead(info, true);
                        }
                        j++;
                    }

                    Thread.Sleep(5000);

                }

                oClient.Quit();
                logMSG = "Connessione terminata ... ";
                this.Invoke(new EventHandler(logAdd));
                Thread.Sleep(5000);

            }
        }

        private void logAdd(object sender, EventArgs e)
        {
            logWrite(logMSG);
        }

        private void connectGmail()
        {
            // Gmail IMAP4 server is "imap.gmail.com"
            oServer = new MailServer("imap.gmail.com", myEmail,
                            Encoding.UTF8.GetString(Convert.FromBase64String(myPwd)),
                            ServerProtocol.Imap4);

            // Enable SSL connection.
            oServer.SSLConnection = true;

            // Set 993 SSL port
            oServer.Port = 993;

        }

        private void connectToRabbit()
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory();
                factory.UserName = "guest";
                factory.Password = "guest";
                factory.VirtualHost = "/";
                factory.Port = 5672;
                factory.HostName = "localhost";

                connection = factory.CreateConnection();
            } catch (Exception ex)
            {
                connection = null;
            }

        }

        private int countMail(MailInfo[] infos)
        {
            int ret = 0;
            for (int i = 0; i < infos.Length; i++)
            {
                MailInfo info = infos[i];

                Mail oMail = oClient.GetMail(info);
                if (oMail.Subject.ToString().Trim().ToLower().Contains("prova rabbitmq"))
                {
                    ret++;
                }
            }
            return ret;
        }

        private void logWrite(string message)
        {
            txtLog.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " - " + message + Environment.NewLine + txtLog.Text;
        }
    }
}
