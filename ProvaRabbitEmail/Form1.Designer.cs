﻿
namespace ProvaRabbitEmail
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetMail = new System.Windows.Forms.Button();
            this.btnStopMail = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnGetMail
            // 
            this.btnGetMail.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetMail.Location = new System.Drawing.Point(15, 369);
            this.btnGetMail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnGetMail.Name = "btnGetMail";
            this.btnGetMail.Size = new System.Drawing.Size(261, 63);
            this.btnGetMail.TabIndex = 0;
            this.btnGetMail.Text = "Start read email";
            this.btnGetMail.UseVisualStyleBackColor = true;
            this.btnGetMail.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnStopMail
            // 
            this.btnStopMail.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopMail.Location = new System.Drawing.Point(593, 369);
            this.btnStopMail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStopMail.Name = "btnStopMail";
            this.btnStopMail.Size = new System.Drawing.Size(260, 63);
            this.btnStopMail.TabIndex = 1;
            this.btnStopMail.Text = "Stop read email";
            this.btnStopMail.UseVisualStyleBackColor = true;
            this.btnStopMail.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.White;
            this.txtLog.Location = new System.Drawing.Point(16, 9);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(837, 355);
            this.txtLog.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(863, 438);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.btnStopMail);
            this.Controls.Add(this.btnGetMail);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "RabbitMQ - Email reader";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetMail;
        private System.Windows.Forms.Button btnStopMail;
        private System.Windows.Forms.TextBox txtLog;
    }
}

