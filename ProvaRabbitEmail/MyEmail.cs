﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.Json;
using System.Text.Json.Serialization;

namespace ProvaRabbitEmail
{
    public class MyEmail
    {
        public DateTime SentDate { get; set; }
        public string Sender { get; set; }
        public string Object { get; set; }
        public string Body { get; set; }
    }
}
